## AUTHOR: 
Jessica Roberts

## ABOUT: 
This program encrypts and decrypts text according to the ADFGX encryption algorithm. Decryption can only be done if the keyword and key alphabet used to encrypt are known.

## REQUIREMENTS:
Python 3.8+

## HOW TO USE:
To use this program, run it in a terminal using python by entering the command "python adfgx_cipher.py" for Windows and "python3 adfgx_cipher.py" for Linux. Upon start, the user will be prompted with three choices: encrypt text, decrypt text, and quit.

Choosing to encrypt will prompt the user for a sentence or word to encrypt. This sentence can have any amount of spacing, punctuation, or capitalization. Once entered, the user will be prompted for a keyword. Single, short words work best as keywords. The keyword can have any amount of spacing, punctuation, or capitalization. Then the user will have another choice: generate secret table or enter secret table. The secret table is a scrambled version of the roman alphabet. Choosing to generate it will randomly scramble the alphabet and then use it for encryption. Entering a secret table requires the user to enter a scrambled version of the alphabet themselves. This option is most useful for testing and reproducing results. After choosing, the program will encrypt the entered text using the given keyword and secret alphabet, and output the resulting encrypted text.

Choosing to decrypt will prompt the user to enter ciphertext that was encrypted using the adfgx cipher. This is only a decryption and not an attack, so the user must know the keyword and secret alphabet used to encrypt the ciphertext. The program also assumes that the ciphertext has had all spaces/punctuation removed and is in all uppercase. After entering the ciphertext, the user will be prompted to enter the keyword and secret alphabet used for encryption. Once entered, the program will decrypt the text and output the resulting plaintext.

Choosing to quit will end the program.