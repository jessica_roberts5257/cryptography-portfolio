from random import shuffle
import string
import sys

'''
Author: Jessica Roberts
Class: Cryptography
Known Bugs: None
Description: This program encrypts and decrypts text using the ADFGX encryption
 algorithm.
'''

ALPHABET = "abcdefghiklmnopqrstuvwxyz"

'''
Input: input, a string; key_alphabet, a string; keyword, a string
Output: a string of encrypted text
Description: This function cleans the input text, and then runs it through the
 polybius square function followed by the columnar transposition function to
 encrypt the text according to the ADFGX algorithm.
'''
def adfgx_encrypt(input, key_alphabet, keyword):
    
    key = clean_key(keyword)
    key = key.upper()
    clean = clean_text(input)

    poly_cipher = polybius_encode(clean, key_alphabet)
    col_trans = col_transpose(poly_cipher, key)

    return col_trans

'''
Input: input, a string; key_alphabet, a string; keyword, a string
Output: a string of decrypted text
Description: This function runs the ciphertext through the reverse columnar
 transposition function followed by the polybius square decode function to
 reverse the steps taken to encode the text according to the ADFGX
 algorithm.
'''
def adfgx_decrypt(input, key_alphabet, keyword):

    key = keyword.upper()

    col_trans = col_reverse(input, key)
    poly_cipher = polybius_decode(col_trans, key_alphabet)

    return poly_cipher

'''
Input: text, a string
Output: a string
Description: This function cleans the input text by removing an spaces or
 punctuation, making the text all lowercase, and replacing any j's with i's
 to account for fitting the roman alphabet into a polybius square.
'''
def clean_text(text):
    result = text.lower().replace(' ', '')
    result = result.translate(str.maketrans('', '', string.punctuation))
    result = result.replace('j', 'i')

    return result

'''
Input: text, a string
Output: a string
Description: This function cleans the key by removing an spaces or
 punctuation.
'''
def clean_key(text):
    result = text.lower().replace(' ', '')
    result = result.translate(str.maketrans('', '', string.punctuation))

    return result

'''
Input: text, a string
Output: a string
Description: This function generates a mixed key alphabet to fill the polybius 
 square by shuffling the characters inside a string conatining all letters of
 the alphabet but j.
'''
def generate_key(text):
    result = ""

    temp = list(text)
    shuffle(temp)

    result = result.join(temp)

    return result

'''
Input: keyword, a string
Output: list
Description: This function takes in a keyword and resturns a list of numbers
 that are the the indeces of the characters within the keyword sorted to be in
 alphabetical order.
'''
def alphabetical_index(keyword):
    size = len(keyword)

    temp = [(keyword[i], i) for i in range(size)]
    temp2 = [(j[1], i) for i,j in enumerate(sorted(temp))]

    result = [k[1] for k in sorted(temp2)]

    return result

'''
Input: keyword, a string
Output: list
Description: This function returns a list of numbers that are the indeces of
 the characters in the keyword returned from alphabetical order to their
 original order.
'''
def orig_index(keyword):
    size = len(keyword)

    temp = [(keyword[i], i) for i in range(size)]

    result = [j[1] for j in sorted(temp)]

    return result

'''
Input: text, a string; keyword, a string
Output: string
Description: This function transposes the input text into a number of columns
 equal to the number of characters in the keyword and then sorts the columns
 in alphabetical order according to the keyword.
'''
def col_transpose(text, keyword):

    size = len(keyword)

    result = ""
    index_list = alphabetical_index(keyword)

    for i in range(size):
        result = result + text[index_list.index(i)::size]

    return result

'''
Input: text, a string; keyword, a string
Output: string
Description: This function takes text that has been organized via columnar
 transposition and first returns it from alphabetical order to its original
 order. It then sorts all of the characters from being in columns to being
 in their original order.
'''
def col_reverse(text, keyword):
    
    string_size = len(text)
    key_size = len(keyword)

    result = ["-"] * string_size
    index_list = orig_index(keyword)

    count = 0
    
    for i in range(key_size):
        current = int(string_size / key_size)

        if(index_list[i] < (string_size % key_size)):
            current = current + 1
        
        result[index_list[i]::key_size] = text[count:count + current]
        count = count + current

    final = ""
    final = final.join(result)

    return final

'''
Input: key_alphabet, a string; a, a character
Output: string
Description: This function navigates a polybius square to find the letter
 code equivalent to a specified character.
'''
def find_letter_index(key_alphabet, a):
    size = 5
    code = 'ADFGX'

    row = int(key_alphabet.index(a) / size)
    col = key_alphabet.index(a) % size

    row_letter = code[row]
    col_letter = code[col]

    return row_letter + col_letter

'''
Input: key_alphabet, a string; letter_index, a string
Output: a character
Description: This function navigates a polybius square to find the character
 equivalent to a letter code.
'''
def find_char(key_alphabet, letter_index):
    size = 5
    code = 'ADFGX'

    row = code.index(letter_index[0])
    col = code.index(letter_index[1])

    character = key_alphabet[row*size + col]

    return character

'''
Input: key_alphabet, a string; plain, a string
Output: string
Description: This function encodes a string of text by assigning a letter code
 to all of its character.
'''
def polybius_encode(plain, key_alphabet):

    result = ""

    for x in plain:
        result = result + find_letter_index(key_alphabet, x)

    return result

'''
Input: key_alphabet, a string; cipher, a string
Output: string
Description: This function decodes a string of text by deciphering each
 character using their letter code.
'''
def polybius_decode(cipher, key_alphabet):
    
    result = ""
    size = len(cipher)

    for i in range(0, size, 2):
        result = result + find_char(key_alphabet, cipher[i:i+2])

    return result

def main():

    choice = -1

    while(choice != 0):

        try:
            choice = int(input("Please make a choice:\n1) Encrypt Text\n2) Decrypt Text\n0) Quit\n"))
        except ValueError:
            print("Must enter an integer.")

        if choice == 1:
            plaintext = input("Please enter text to encrypt: ")
            keyword = input("Please enter keyword: ")

            choice2 = 0

            while(choice2 != 1 and choice2 != 2):
                try:
                    choice2 = int(input("1) Generate secret table\n2) Enter secret table\n"))
                except ValueError:
                    print("Must enter an integer.")

            if choice2 == 1:
                print("Generating key table...")
                key_string = generate_key(ALPHABET)

            if choice2 == 2:
                key_string = input("Enter key alphabet: ")

            print("Key Alphabet: " + key_string)
            ciphertext = adfgx_encrypt(plaintext, key_string, keyword)
            print("Encrypted Text: " + ciphertext)
        
        if choice == 2:
            ciphertext = input("Please enter text to decrypt: ")
            keyword = input("Please enter keyword: ")
            key_string = input("Please enter key alphabet used to encrypt: ")

            plaintext = adfgx_decrypt(ciphertext, key_string, keyword)
            print("Decrypted Text: " + plaintext)

        elif choice != 1 and choice != 2 and choice != 0: 
            print("Invalid selection. Please restrict input to the menu options.")

main()