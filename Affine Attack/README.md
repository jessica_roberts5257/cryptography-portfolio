## AUTHOR: 
Jessica Roberts

##  ABOUT: 
This program performs a known plaintext attack on ciphertext encrypted using the affine cipher.

## REQUIREMENTS:
Python 3.8+
Cryptomath Library

## HOW TO USE:
**NOTE**: This program requires at least the first two letters of the plaintext to be known to launch its attack.


To use this program, run it in a terminal using python by entering the command “python affine_attack.py” for Windows and “python3 affine_attack.py” for Linux. Upon start, the user will be prompted to enter a ciphertext that was encrypted using the affine cipher. It assumes that the ciphertext will be given with no spaces or punctuation, and in all uppercase. After entering the ciphertext the user will be prompted to enter at least the first two letters of known plaintext in uppercase. It is important that the plaintext letters be given in order. After entering, the program will output the best guess at the two key numbers used to encrypt the ciphertext. These key numbers could then be used in an affine decryption function to decrypt the ciphertext.