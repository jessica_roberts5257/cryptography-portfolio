import string
import cryptomath
from cryptomath import functions

'''
Author: Jessica Roberts
Class: Cryptography
Known Bugs: None
Description: This program performs a known plaintext attack on ciphertext
 that was encrypted using the affine cipher.
'''

ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

'''
Input: input, string of ciphertext; key, tuple of encryption integers
Output: string of descrypted text
Description: This function takes a string of ciphertext and decrypts it if
#the encryption key is known.
'''
def affine_decrypt(input, key):
    
    temp = []
    output = ""

    inverse = functions.findModInverse(key[0], 26)

    for x in input:
        new_char = chr(((inverse * (ord(x) - ord('A') - key[1])) % 26) + ord('A'))
        temp.append(new_char)

    return output.join(temp)

'''
Input: cipher, a string; plain, a string
Output: a list of integers
Description: This function uses the ciphertext and known letters of the
 plaintext to determine what integer values were used to encrypt the
 ciphertext.
'''
def solve_system(cipher, plain):
    
    key = []

    p = find_letter_index(plain[0])
    q = find_letter_index(plain[1])
    r = find_letter_index(cipher[0])
    s = find_letter_index(cipher[1])

    d = (p - q)
    d1 = functions.findModInverse(d, 26)

    alpha = (d1*(r - s)) % 26
    beta = (d1*(p*s - q*r)) % 26

    key.append(alpha)
    key.append(beta)

    return key

'''
Input: a, a character
Output: an integer
Description: This function finds a given character's numerical location in the
 alphabet.
'''
def find_letter_index(a):
    count = 0

    for x in ALPHABET:
        if(a == x):
            return count
        count = count + 1
    
    return -1

def main():

    ciphertext = input("Enter ciphertext: ")
    plain = input("Please enter at least the first two letters of known plaintext (uppercase): ")

    key = solve_system(ciphertext, plain)
    print(key)

main()