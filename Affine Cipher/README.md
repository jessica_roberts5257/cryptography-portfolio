## AUTHOR: 
Jessica Roberts

## ABOUT: 
This program encrypts and decrypts text according to the affine encryption algorithm. Decryption can only be done if both key values used to encrypt are known.

## REQUIREMENTS:
Python 3.8+
Cryptomath Library

## HOW TO USE:
To use this program, run it in a terminal using python by entering the command "python affine_cipher.py" for Windows and "python3 affine_cipher.py" for Linux. Upon start, the user will be prompted with three choices: encrypt text, decrypt text, and quit.

Choosing to encrypt will prompt the user for a sentence or word to encrypt. This sentence can have any amount of spacing, punctuation, or capitalization. Once entered, the user will be prompted for the first key number. Due to the nature of the affine function, the values for the first integer are limited to numbers coprime to 26. The following is a list of valid "a" values: 1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, and 25. After entering, the user will be prompted to enter the second key number. This can be any number, but it is unecessary to use any number above 25 as this number will be modded by 25 causing a "wrap around" effect that always makes the value of this key number from 0-25. After this, the program will encrypt the given text using the chosen key numbers and output the resulting ciphertext.

Choosing to decrypt will prompt the user to enter ciphertext that was encrypted using the affine cipher. This is only a decryption and not an attack, so the user must know the two key numbers used to encrypt the ciphertext. The program also assumes that the ciphertext has had all spaces/punctuation removed and is in all uppercase. After entering the ciphertext, the user will be prompted to enter the two key numbers used for encryption. Once entered, the program will decrypt the text and output the resulting plaintext.

Choosing to quit will end the program.