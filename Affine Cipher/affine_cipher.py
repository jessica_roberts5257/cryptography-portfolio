import sys
import cryptomath
from cryptomath import functions
import string

#Author: Jessica Roberts
#Class: Cryptography
#Description: The purpose of this program is to encrypt and decrypt text using
#an affine cipher. This program only can decrypt is the key used to encrypt is
#known.
#Known Bugs: None

#Input: input, string of plain text
#Output: Clean text string
#Description: Takes a string of text and removes spaces, punctuation, and makes
#all characters uppercase.
def clean_text(input):
    text = input.upper().replace(' ', '')
    text = text.translate(str.maketrans('', '', string.punctuation))

    return text

#Input: input, string of text; key, tuple of encryption integers
#Output: string of encrypted text
#Description: This function takes in plaintext and a chosen key and encrypts
#the text using an affine cipher.
def affine_encrypt(input, key):
    
    temp = []
    output = ""

    text = clean_text(input)

    for x in text:
        new_char = chr(((key[0] * (ord(x) - ord('A')) + key[1]) % 26) + ord('A'))
        temp.append(new_char)

    return output.join(temp)

#Input: input, string of ciphertext; key, tuple of encryption integers
#Output: string of descrypted text
#Description: This function takes a string of ciphertext and decrypts it if
#the encryption key is known.
def affine_decrypt(input, key):
    
    temp = []
    output = ""

    inverse = functions.findModInverse(key[0], 26)

    for x in input:
        new_char = chr(((inverse * (ord(x) - ord('A') - key[1])) % 26) + ord('A'))
        temp.append(new_char)

    return output.join(temp)

def main():

    choice = -1

    while(choice != 0):
        try:
            choice = int(input("Please make a choice: \n1) Encrypt Text\n2) Decrypt Text\n"
            + "0) Quit\n"))
        except ValueError:
            print("Must enter an integer.")

        if choice == 1:
            text = input("Enter text to encrypt: ")
            key = [-1, -1]

            while(key[0] == -1 or key[1] == -1):
                try:
                    key[0] = int(input("Enter integer relatively prime to 26 (0-25): "))
                except:
                    print("Must enter an integer.")
                    key[0] = -1
                try:
                    key[1] = int(input("Enter second integer: "))
                except:
                    print("Must enter an integer.")
                    key[1] = -1

            ciphertext = affine_encrypt(text, key)

            print("Plaintext: " + text)
            print("Ciphertext: " + ciphertext)
        
        if choice == 2:
            text = input("Enter text to decrypt: ")
            key[0] = int(input("Enter first key integer: "))
            key[1] = int(input("Enter second key integer: "))

            plaintext = affine_decrypt(text, key)

            print("Ciphertext: " + text)
            print("Plaintext: " + plaintext)

main()