## AUTHOR: 
Jessica Roberts

## ABOUT: 
The purpose of this library is to provide common functions used in cryptomath to reduce repeated code in different encryption and decryption projects.

## AVAILABLE FUNCTIONS: 
* Greatest Common Divisor (gcd)
* Extended Eucildean Algorithm (extendedgcd)
* Modular Multiplicative Inverse of an Integer (findModInverse)
* Finding the Squares of an Integer Mod N (findModSquares)
* Is Prime (is_prime)
* Random Prime (random_prime)
* Integer to a Power Mod N (modPow)
* Factor (factor)

## REQUIREMENTS:
Python 3.8+

## HOW TO USE:
To install, run "pip install path/to/Cryptomath Library/dist/cryptomath-0.1.4-py3-none-any.whl".
Then, in any program where you want to utilize this library, use "import cryptomath" and "from cryptomath import functions". To test each function, run "python setup.py pytest".

### NOTE: 
In some IDE's python will act as if the cryptomath library did not import properly, even though the functions can still be used. This is not necessary, but to make the IDE recognize the library properly, you can add the path to the "Cryptomath Library" folder in the "extra paths" sections of your workspace settings.

## FUNCTION USE:

* **Greatest Common Divisor**: This function accepts two arguments that are the two integers from which you wish to obtain the greatest common divisor. This function then returns a single integer that is the greatest common divisor.

* **Extended Eucildean Algorithm**: This function accepts two arguments that are the two integers you want to calculate. It returns three values: an integer that is the greatest common divisor, the first diophantine solution, and the second diophantine solution.

* **Modular Multiplicative Inverse of an Integer**: This function accepts two arguments, the first being the integer you want to find the inverse of, and the second being an integer indicating the modulus. It returns a single integer that is the modular inverse.

* **Finding the Squares of an Integer Mod N**: This function accepts two arguments, the first being the integer you want to find the squares of, and the second being an integer indicating the modulus. It returns a list of all integers found to be squares of the first argument in the required modulus.

* **Is Prime**: This function accepts one argument that is the integer you want to check for prime status. It returns a boolean that is true if the number is prime and false if the number is not.

* **Random Prime**: This function accepts one argument b that determines the range from which to choose a prime from such that the prime will be between 2<sup>b</sup>-1 and 2<sup>b + 1</sup>-1. It returns a single number that is a random prime number within that range.

* **Integer to a Power Mod N**: This function accepts three arguments, the first being an integer that is the exponent base, the second being an integer that is the exponent, and the third being an integer indicating the modulus. It returns a single integer.

* **Factor**: This function accepts two arguments, the first being the composite integer you want to factor, the second being an integer indicating the factorization method. If the second argument is set to 1, it will factor using Fermat's method, if set to 2, it will factor using the pollard rho algorithm, and if set to 3, it will factor using the pollard p-1 algorithm. It returns a list of all prime factors of a composite integer.

    **NOTE**: For fermat's method, the function assumes the composite number is odd, as this method works only on odd numbers.