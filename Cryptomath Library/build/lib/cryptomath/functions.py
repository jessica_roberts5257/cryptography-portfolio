import random
from math import ceil, sqrt

#Author: Jessica Roberts
#Class: Cryptography
#Description: This file contains all functions in the cryptomath library
#Known Bugs: None

#Greatest Common Divisor
#Input: a, first number; b, second number
#Output: returns the greatest number that divides a and b
#Description: This function finds the greatest common divisor
# between two numbers
def gcd(a, b):

    if a == 0:
        return b
    if b == 0:
        return a
    else:
        r = a % b
        return gcd(b, r)

#Greatest Common Divisor Extended
#Input: a, first coefficient; n, second coefficient
#Output: returns the greatest number that divides a and b, 
#the x value, and the y value
#Description: This function solves for the x and y values of a diophantine
# equation, as well as finds the gcd of the two coefficients.
def extendedgcd(a, b):

    x1, x0 = 1, 0
    y1, y0 = 0, 1
    q = a // b
    temp, temp2 = b, a % b

    while temp2:
        x1, x0 = x0, x1 - q * x0
        y1, y0 = y0, y1 - q * y0
        q = temp // temp2
        temp, temp2 = temp2, temp % temp2

    return temp, x0, y0

#Find Modular Multiplicative Inverse
#Input: a, integer; n, mod
#Output: returns modular inverse
#Description: This function find the modular multiplicative inverse of an
# integer a mod n.
def findModInverse(a, n):
    d, x, y = extendedgcd(a, n)

    if d == 1:
        return x % n
    else:
        return -1

#Find Squares of Integer Mod N
#Input: a, integer; n, mod
#Output: returns list of squares
#Description: This function finds all squares of an integer mod n.
def findModSquares(a, n):

    squares = []

    m = a % n

    for x in range(2, n):
        if((x * x) % n == m):
            squares.append(x)

    return squares

#Miller Rabin Primality Test
#Input: n, integer
#Output: boolean
#Description: This function uses the Miller Rabin algorithm for determining
# primality and returns True if the number is prime and False if the number
# is not prime.
def miller_rabin(n):
    q = n - 1
    k = 0

    while not q & 1:
        q >>= 1
        k += 1

    temp = random.randint(2, n - 2)

    if modPow(temp, q, n) == 1:
        return True

    for i in range(k):
        if modPow(temp, q << i, n) == (n - 1):
            return True

    return False

#Find if Number is Prime
#Input: n, integer; p, float
#Output: boolean
#Description: This functions returns true if a given number is prime
# and false if the number is not prime.
def is_prime(n, p):

    if (n == 2 or n == 3):
        return True

    flag = 1

    while flag >= p:
        if not miller_rabin(n):
            return False
        flag = flag * (1 / 4)
    
    return True

#Find Random Prime Within Range
#Input: b, an integer
#Output: returns a prime number
#Description: This function finds all prime numbers within a given range
# and return a random one among them.
def random_prime(b):

    result = 0
    for i in range(b - 1):

        if random.random() < 0.5:
            result ^= 1
        else:
            result ^= 0

        result <<= 1

    result ^= 1

    return result

#Power Mod 
#Input: a, integer; b, power; n, modulus
#Output: Returns an integer
#Description: This functions calculates an integer to a power mod n
def modPow(a, b, n):
    result = 1
    a %= n

    while b:
        if b & 1:
            result = (result * a) % n
        a = (a * a) % n
        b >>= 1

    return result

#Factor Large Composite
#Input: n, integer; m, integer indicating which method to use
#Output: returns factors
#Description: This function finds the factors of a large composite number using
# threee different methods of factorization. If m = 1, it uses the fermat 
# method, if m = 2, it uses the pollard rho method, and if m = 3, it uses the
# pollard p - 1 method.
def factor(n, m):

    result = []
    
    if(m == 1):
        result = fermat(n)

    if(m == 2):
        temp = n

        while(True):
            x = pollard_rho(n)
        
            result.append(x)

            reduce = int(temp/x)

            if(is_prime(reduce, 0.0001)):
                result.append(reduce)
                break
            else:
                temp = reduce

    if(m == 3):
        temp = n

        while(True):
            x = pollard_p1(temp)

            result.append(x)

            reduce = int(temp/x)

            if(is_prime(reduce, 0.0001)):
                result.append(reduce)
                break
            else:
                temp = reduce
    
    return result

#Factor Using Fermat
#Input: n, a composite integer
#Output: returns list of factors
#Description: This function factors a known composite integer using fermat's
# factorization method and returns a list of all factors.
def fermat(n):

    x = ceil(sqrt(n))
    temp = x * x

    if (temp == n):
        return [x, x]
    
    while(True):
        b2 = x * x - n
        temp2 = int(sqrt(b2))

        if(temp2 * temp2 == b2):
            break
        else:
            x = x +1

    result = [x - temp2, x + temp2]

    return result

#Factor Using Pollard Rho
#Input: n, a composite integer
#Output: returns a factor of n
#Description: This function factors a known composite integer using the
# pollard rho factorization method and returns one factor of n.
def pollard_rho(n):

    if(n == 1):
        return n
    
    if ( n % 2 == 0):
        return 2

    b1 = (random.randint(0, 2) % (n - 2))
    b2 = b1
    c = (random.randint(0, 1) % (n - 1))

    temp = 1

    while(temp == 1):
        b1 = (modPow(b1, 2, n) + c + n) % n

        b2 = (modPow(b2, 2, n) + c + n) % n
        b2 = (modPow(b2, 2, n) + c + n) % n

        temp = gcd(abs(b1 - b2), n)

        if(temp == n):
            return pollard_rho(n)

    return temp

#Factor Using Pollard P - 1
#Input: n, a composite integer
#Output: returns a factor of n
#Description: This function factors a known composite integer using the
# pollard p - 1 factorization method and returns a factor of n.
def pollard_p1(n):
    
    b = 2
    e = 1

    while(True):
        b = modPow(b, e, n)

        temp = gcd((b-1), n)

        if(temp > 1):
            return temp
        
        e = e + 1