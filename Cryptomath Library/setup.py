from setuptools import find_packages, setup

setup(
    name='cryptomath',
    packages=find_packages(include=['cryptomath']),
    version='0.1.4',
    description='A library of useful cryptomath functions',
    author='Jessica Roberts',
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests'
)