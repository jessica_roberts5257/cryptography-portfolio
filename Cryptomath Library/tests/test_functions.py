import sys
from cryptomath import functions
import unittest

#Author: Jessica Roberts
#Class: Cryptography
#Description: This file contains all test functions in the cryptomath library
#Known Bugs: None

#GCD Test
def test_gcd():

    assert functions.gcd(376, 424) == 8

    assert functions.gcd(542, 798) == 2

    assert functions.gcd(1045, 1049) == 1

    assert functions.gcd(24, 36) == 12

    assert functions.gcd(36, 24) == 12

#Extended GCD Test
def test_extendedgcd():
    d, x, y = functions.extendedgcd(45, 15)
    assert d == 15
    assert x == 0
    assert y == 1

    d, x, y = functions.extendedgcd(37, 42)
    assert d == 1
    assert x == -17
    assert y == 15

    d, x, y = functions.extendedgcd(24, 36)
    assert d == 12
    assert x == -1
    assert y == 1

    d, x, y = functions.extendedgcd(512, 308)
    assert d == 4
    assert x == -3
    assert y == 5

#Modular Inverse Test
def test_findModInverse():
    assert functions.findModInverse(4, 11) == 3

    assert functions.findModInverse(16, 3) == 1

    assert functions.findModInverse(307, 103) == 51

    assert functions.findModInverse(57, 34) == 3

    assert functions.findModInverse(12, 2) == -1

#Modular Squares Test
def test_findModSquares():
    answers = [3, 4]
    assert functions.findModSquares(2, 7) == answers

    answers = [5, 8]
    assert functions.findModSquares(12, 13) == answers

    answers = [40]
    assert functions.findModSquares(42, 41) == answers

    answers = []
    assert functions.findModSquares(6, 31) == answers

#Is Prime Test
def test_is_prime():
    assert functions.is_prime(2, 0.0001) == True

    assert functions.is_prime(47, 0.0001) == True

    assert functions.is_prime(6, 0.0001) == False

    assert functions.is_prime(9, 0.0001) == False

    assert functions.is_prime(5003, 0.0001) == True

def test_modPow():
    assert functions.modPow(2, 64, 7) == 2

    assert functions.modPow(3, 67, 4) == 3

    assert functions.modPow(72, 2, 5) == 4

    assert functions.modPow(163, 82, 9) == 1

    assert functions.modPow(33398, 47, 12) == 8

#Fermat Factorization Test
def test_fermat():
    assert functions.factor(33, 1) == [3, 11]

    assert functions.factor(153, 1) == [9, 17]

    assert functions.factor(6559, 1) == [7, 937]

    assert functions.factor(3379, 1) == [31, 109]

    assert functions.factor(15, 1) == [3, 5]

#Pollard Rho Factorization Test
def test_pollard_rho():
    assert functions.factor(33, 2) == [3, 11]

    assert functions.factor(153, 2) == [3, 3, 17]

    assert functions.factor(6559, 2) == [7, 937]

    assert functions.factor(3379, 2) == [31, 109]

    assert functions.factor(15, 2) == [3, 5]

#Pollard P-1 Factorization Test
def test_pollard_p1():
    assert functions.factor(33, 3) == [3, 11]

    assert functions.factor(153, 3) == [3, 3, 17]

    assert functions.factor(6559, 3) == [7, 937]

    assert functions.factor(3379, 3) == [31, 109]

    assert functions.factor(15, 3) == [3, 5]