import sys
sys.path.insert(1, "C:/Users/7522998/Documents/Crypto/Cryptography Portfolio/")
from DES import des_64

'''
Author: Jessica Roberts
Class: Cryptography
Known Bugs: None
Description: This program performs a differential cryptanalysis on three rounds
 of the full DES algorithm. 
'''

grid = list()

'''
Input: info, an integer; rounds, an integer
Output: an integer
Description: Performs three rounds of DES encryption on an integer.
'''
def des_3_round_encrypt(info, rounds):
    left, right = des_64.separate(info, 64, 2)

    for i in range(rounds):
        temp = right
        right = left ^ des_64.des_round(right, des_64.keys[i])
        left = temp

    result = des_64.merge((right, left), 32)
    return result

'''
Input: original, an integer
Output: an integer
Description: Verifies the validity of a key.
'''
def clean_key(original):

    result = original

    for i in range(8):
        temp = 1
        for j in range(7):
            temp2 = des_64.bit_num(original, 8 * (7 - i) + (7 - j))
            temp ^= temp2
        result ^= (temp << (8 * (7 - i)))

    return result

'''
Input: None
Output: None
Description: Creates a grid of possible s box inputs and outputs.
'''
def generate_grid():

    for i in range(8):
        grid.append(list())
        for j in range(2**6):
            grid[i].append(list())
            for k in range(2**4):
                grid[i][j].append(list())
            for l in range(2**6):
                temp = des_64.get_val_in_sbox(l, des_64.SBOX[i]) ^ des_64.get_val_in_sbox(l ^ j, des_64.SBOX[i])
                grid[i][j][temp].append(l)

'''
Input: grid, a list
Output: a list
Description: This function reverses a table of values
'''
def rev_grid(grid):
    temp_grid = list()
    temp = list()

    for i, j in enumerate(grid):
        temp.append((i + 1, j))

    temp.sort(key=lambda x:x[1])

    for i, j in temp:
        temp_grid.append(i)

    return temp_grid

'''
Input: i, an integer; j, an integer; k, an integer; l, an integer
Output: a list
Description: This function creates a table of possible s box inputs based on
 an output value.
'''
def generate_box(i, j, k, l):

    result = list()

    for x in grid[i][j ^ k][l]:
        temp = x ^ j
        result.append(temp)

    return result

'''
Input: plain, an integer; cipher, an integer
Output: list
Description: Performs analysis on chunks of plaintext and ciphertext to 
 determine possible key values based on their differences.
'''
def differential(plain, cipher):
    boxes = list()

    plain1, plain2 = plain
    cipher1, cipher2 = cipher

    L, R = des_64.separate(plain1, 64, 2)
    L2, R2 = des_64.separate(plain2, 64, 2)

    if (R == R2):
        R3, L3 = des_64.separate(cipher1, 64, 2)
        R4, L4 = des_64.separate(cipher2, 64, 2)

        rexor = R3 ^ R4
        lexor = L ^ L2

        c = des_64.permute(rexor ^ lexor, rev_grid(des_64.PERMUTE), 32)
        a = des_64.permute(L3, des_64.EP, 32)
        b = des_64.permute(L4, des_64.EP, 32)

        splitc = des_64.separate(c, 32, 8)
        splita = des_64.separate(a, 48, 8)
        splitb = des_64.separate(b, 48, 8)

        for i in range(8):
            temp = generate_box(i, splita[i], splitb[i], splitc[i])

            boxes.append(temp)

    return boxes

'''
Input: plaintext, an integer; ciphertext, an integer
Output: an integer
Description: Uses helper function to perform differential cryptanalysis on 
 3-round DES and returns the key.
'''
def function(plaintext, ciphertext):
    key_list = list()
    diff_table = differential(plaintext[0], ciphertext[0])

    for i in range(8):
        key_list.append(set(diff_table[i]))
    
    for i in range(1, len(plaintext)):
        diff_table = differential(plaintext[i], ciphertext[i])

        for j in range(8):
            key_list[j] = key_list[j].intersection(set(diff_table[j]))
    
    bits = list([[0]])

    for i in range(8):
        for j in range(len(bits)):
            kbit = bits.pop(0)
            for k in key_list[i]:
                bits.append(kbit + [k])

    keys = list()

    for x in bits:
        temp = des_64.merge(x, 6)
        keys.append(temp)

    KP_Ext = [8, 16, 24, 32, 40, 48, 56, 64] + des_64.KP
    KP_Ext_Rev = rev_grid(KP_Ext)

    KP2_Ext = [9, 18, 22, 25, 35, 38, 43, 54] + des_64.KP2
    KP2_Ext_Rev = rev_grid(KP2_Ext)

    for i in range(2**8):
        for x in keys:
            key = des_64.merge((i, x), 48)
            key = des_64.permute(key, KP2_Ext_Rev, 56)

            left, right = des_64.separate(key, 56, 2)

            for j in range(3):
                temp = des_64.right_by_n(left, 28, des_64.SHIFT[j])
                temp2 = des_64.right_by_n(right, 28, des_64.SHIFT[j])
                left, right = temp, temp2

            key = des_64.merge((left, right), 28)
            key = des_64.permute(key, KP_Ext_Rev, 64)
            key = clean_key(key)

            des_64.generate_keys(key, 3)
            test_cipher = des_3_round_encrypt(plaintext[0][0], 3)

            if test_cipher == ciphertext[0][0]:
                return key
    
    return -1

if __name__ == '__main__':

    original_key = 0xFE326232EA6D0D73

    des_64.generate_keys(original_key, 3)
    generate_grid()

    data = [
        (0x748502CD38451097, 0x3874756438451097),
        (0x357418DA013FEC86, 0x33549847013FEC86)
    ]

    ciphertext = list()

    print("Original key: ")
    print(hex(original_key))

    print("\nMessage data:")

    for a, b in data:
        print(hex(a), hex(b))
        ciphertext.append((des_3_round_encrypt(a, 3), des_3_round_encrypt(b, 3)))

    print("\nEncrypted data:")
    for x in ciphertext:
        print(hex(x[0]).upper(), hex(x[1]).upper())

    analysis = function(data, ciphertext)

    print("\nCryptanalyzed key:")
    print(hex(analysis))
