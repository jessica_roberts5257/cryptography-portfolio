## AUTHOR: 
Jessica Roberts

## ABOUT:
The purpose of this program is to perform differential cryptanalysis of three rounds of the DES algorithm.

## REQUIREMENTS:
Python 3.8+

## HOW TO USE:
To run this program using a terminal, use the command "python 3_round_diff.py" for Windows and "python3 3_round_diff.py" for Linux. Key and plaintext have been hardcoded in hexadecimal format. To test different values, edit "original_key" and "data" values in main function.