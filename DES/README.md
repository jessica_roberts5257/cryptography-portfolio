## AUTHOR: 
Jessica Roberts

## ABOUT:
The purpose of this program is to encrypt and decrypt data using the full 64-bit DES algorithm.

## REQUIREMENTS:
Python 3.8+

## HOW TO USE:
To run this program using a terminal, use the command "python des_64.py" for Windows and "python3 des_64.py" for Linux. Key and plaintext have been hardcoded in hexadecimal format. To test different values, edit "key" and "message" values in main function.