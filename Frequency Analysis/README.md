## AUTHOR: 
Jessica Roberts

## ABOUT:
The purpose of this program is to perform a frequency analysis on a string of text.

## REQUIREMENTS:
G++ Compiler or C++ IDE

## HOW TO USE:
**NOTE**: 
Input ciphertext must contain at least 500 characters, be free of all punctutation and whitespace, and be in all uppercase.


This program uses command line arguments to read in the ciphertext from a file. Place desired file into the same location as main.cpp. If using G++, open a terminal, navigate to the location of main.cpp, and enter "g++ -o <executable_name> main.cpp". Then, enter "./<executable_name> filename.txt". If using a C++ IDE, choose to run without debugging and enter the text file into the debugging properties.