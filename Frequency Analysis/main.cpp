#include "iostream"
#include "string"
#include "fstream"
#include "algorithm"

/******************************************************************************
* Author: Jessica Roberts
* Class: Cryptography
* Known Bugs: None
* Description: The purpose of this program is to launch an attack on 
* substitution ciphers by comparing the frequencies of the encrypted letters 
* to frequencies of letters in the English language. This way, it is easy to
* guess which letters the encrypted letters map back to.
******************************************************************************/

using namespace std;

const char ALPHABET[26] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

void frequency_Analysis(string input, double arr[]);

/******************************************************************************
* Input: input, a string; arr, an array of doubles
* Output: None
* Description: This function calculates the letter frequencies in a string of
* text and inserts them into an array of doubles in alphabetical order.
******************************************************************************/
void frequency_Analysis(string input, double arr[])
{

    for (int i = 0; i < input.length(); i++)
    {
        int charValue = (int)input[i] - 97;
        if (charValue >= 0 && charValue <= 25)
            arr[charValue]++;
    }

}

int main(int argc, char* argv[])
{
    string input;
    ifstream fin;
    double frequencies[26];

    if (argc != 2)
    {
        cout << "Usage: Frequency Analysis.exe inputfile" << endl;
        return -1;
    }

    fin.open(argv[1]);

    if (!fin.is_open())
    {
        cout << "File " << argv[1] << " failed to open." << endl;
        fin.close();
        return -1;
    }

    fin >> input;

    cout << "Input: " << input << endl;

    transform(input.begin(), input.end(), input.begin(), ::tolower);

    for (int i = 0; i < 26; i++)
        frequencies[i] = 0;

    frequency_Analysis(input, frequencies);

    for (int i = 0; i < 26; i++)
        cout << ALPHABET[i] << ": " << frequencies[i] << endl;
}