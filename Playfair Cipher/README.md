## AUTHOR: 
Jessica Roberts

## ABOUT: 
This program encrypts and decrypts text using the playfair encryption algorithm.

## REQUIREMENTS:
C IDE or GCC compiler

## HOW TO USE:
To use this program using a terminal, navigate to the "playfairCipher.exe" executable, and run the command "playfairCipher." Otherwise, simply click on the playfairCipher.exe executable to run. Upon start, the user will be prompted with three choices: encrypt text, decrypt text, and quit.

Choosing to encrypt will prompt the user for a sentence or word to encrypt. This sentence can have any amount of spacing, punctuation, or capitalization. Once entered, the user will be prompted for a keyword. Single, short words work best as keywords. The keyword can have any amount of spacing, punctuation, or capitalization. After entering, the program will encrypt the entered text using the given keyword and output the resulting encrypted text.

Choosing to decrypt will prompt the user to enter ciphertext that was encrypted using the playfair cipher. This is only a decryption and not an attack, so the user must know the keyword used to encrypt the ciphertext. The program also assumes that the ciphertext has had all spaces/punctuation removed and is in all lowercase. After entering the ciphertext, the user will be prompted to enter the keyword used for encryption. Once entered, the program will decrypt the text and output the resulting plaintext.

Choosing to quit will end the program.