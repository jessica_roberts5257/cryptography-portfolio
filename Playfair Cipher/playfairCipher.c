#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdbool.h"
#include "ctype.h"

/******************************************************************************
 * Author: Jessica Roberts
 * Class: Cryptography
 * Known Bugs: None
 * Description: This function encrypts and decrypts text using the playfair
 * encryption algorithm. This function can only decrypt if the keyword used
 * to encrypt is known.
 *****************************************************************************/

void cleanString(char text[]);
void makeLower(char text[]);
void makeLengthEven(char text[]);
void makeAlphabetTable(char key[], char keyTable[5][5]);
void findDigraph(char keyTable[5][5], char a, char b, int arr[]);
void playfairEncrypt(char text[], char keyTable[5][5]);
void playfairDecrypt(char text[], char keyTable[5][5]);

/******************************************************************************
 * Input: text, a string
 * Output: none
 * Description: This function cleans a string of text by removing all spaces
 * and punctuation.
 *****************************************************************************/
void cleanString(char text[])
{
    int size = strlen(text);
    int count = 0;

    for (int i = 0; i < size; i++)
        if (text[i] != ' ' && !(ispunct(text[i])))
            text[count++] = text[i];

    text[count] = '\0';
}

/******************************************************************************
 * Input: text, a string
 * Output: none
 * Description: This function converts a string of text to all lowercase.
 *****************************************************************************/
void makeLower(char text[])
{
    int size = strlen(text);

    for(int i = 0; i < size; i++)
    {
        text[i] = tolower(text[i]);
    }
}

/******************************************************************************
 * Input: text, a string
 * Output: none
 * Description: This function checks the length of a string of text and, if it
 * is odd, inserts a filler character to make the length even.
 *****************************************************************************/
void makeLengthEven(char text[])
{
    int size = strlen(text);

    if(size % 2 != 0)
    {
        text[size++] = 'z';
        text[size] = '\0';
    }
}

/******************************************************************************
 * Input: key, a string; keytable, a 2D character array
 * Output: none
 * Description: This function creates a polybius square using the keyword and
 * then filling the remaining spaces with the rest of the alphabet, not
 * including letters in the keyword.
 *****************************************************************************/
void makeAlphabetTable(char key[], char keyTable[5][5])
{
    int size = strlen(key);
    int *alpha;
 
    alpha = (int*)calloc(26, sizeof(int));
    for (int i = 0; i < size; i++) {
        if (key[i] != 'j')
            alpha[key[i] - 97] = 2;
    }
 
    alpha['j' - 97] = 1;
 
    int i = 0;
    int j = 0;
 
    for (int k = 0; k < size; k++) {
        if (alpha[key[k] - 97] == 2) {
            alpha[key[k] - 97] -= 1;
            keyTable[i][j] = key[k];
            j++;
            if (j == 5) {
                i++;
                j = 0;
            }
        }
    }
 
    for (int k = 0; k < 26; k++) {
        if (alpha[k] == 0) {
            keyTable[i][j] = (char)(k + 97);
            j++;
            if (j == 5) {
                i++;
                j = 0;
            }
        }
    }
}

/******************************************************************************
 * Input: keyTable, a 2D character array; a, a character; b, a character;
 * arr, an integer array
 * Output: none
 * Description: This function uses the key polybius square to find the digraph
 * equivalent of the two input characters.
 *****************************************************************************/
void findDigraph(char keyTable[5][5], char a, char b, int arr[])
{
 
    if (a == 'j')
        a = 'i';
    if (b == 'j')
        b = 'i';
 
    for (int i = 0; i < 5; i++) {
 
        for (int j = 0; j < 5; j++) {
 
            if (keyTable[i][j] == a) {
                arr[0] = i;
                arr[1] = j;
            }
            if (keyTable[i][j] == b) {
                arr[2] = i;
                arr[3] = j;
            }
        }
    }
}

/******************************************************************************
 * Input: text, a string; keyTable, a 2D character array
 * Output: none
 * Description: This function encrypts plaintext using a key polybius square.
 *****************************************************************************/
void playfairEncrypt(char text[], char keyTable[5][5])
{
    int temp[4];
    int size = strlen(text);
 
    for (int i = 0; i < size; i += 2) {

        findDigraph(keyTable, text[i], text[i + 1], temp);
 
        if (temp[0] == temp[2]) {
            text[i] = keyTable[temp[0]][(temp[1] + 1) % 5];
            text[i + 1] = keyTable[temp[0]][(temp[3] + 1) % 5];
        }
        else if (temp[1] == temp[3]) {
            text[i] = keyTable[(temp[0] + 1) % 5][temp[1]];
            text[i + 1] = keyTable[(temp[2] + 1) % 5][temp[1]];
        }
        else {
            text[i] = keyTable[temp[0]][temp[3]];
            text[i + 1] = keyTable[temp[2]][temp[1]];
        }
    }
}

/******************************************************************************
 * Input: text, a string; keyTable, a 2D character array
 * Output: none
 * Description: This function decrypts ciphertext using a key polybius square.
 *****************************************************************************/
void playfairDecrypt(char text[], char keyTable[5][5])
{
    int temp[4];
    int size = strlen(text);

    for(int i = 0; i < size; i+=2)
    {
        findDigraph(keyTable, text[i], text[i + 1], temp);

        if(temp[0] == temp[2])
        {
            text[i] = keyTable[temp[0]][(temp[1] - 1) % 5];
            text[i + 1] = keyTable[temp[0]][(temp[3] - 1) % 5];
        }
        else if(temp[1] == temp[3])
        {
            text[i] = keyTable[(temp[0] - 1) % 5][temp[1]];
            text[i + 1] = keyTable[(temp[2] - 1) % 5][temp[1]];
        }
        else
        {
            text[i] = keyTable[temp[0]][temp[3]];
            text[i + 1] = keyTable[temp[2]][temp[1]];
        }
    }
}

int main()
{
    int choice = -1;
    char text[100];
    char keyword[30];
    char keyTable[5][5];

    while(choice != 0)
    {
        printf("\nPlease make a choice:\n1) Encrypt Text\n2) Decrypt Text\n0) Quit\n");
        if(scanf("%d", &choice))
        {
            if(choice == 1)
            {
                while ((getchar()) != '\n');
                printf("\nPlease enter text to encrypt: ");
                gets(text);
                printf("Please enter keyword: ");
                gets(keyword);

                cleanString(text);
                makeLower(text);
                makeLengthEven(text);

                cleanString(keyword);
                makeLower(keyword);

                makeAlphabetTable(keyword, keyTable);

                playfairEncrypt(text, keyTable);

                printf("Encrypted text: %s\n", text);
            }
            else if(choice == 2)
            {
                while ((getchar()) != '\n');
                printf("\nPlease enter text to decrypt: ");
                gets(text);
                printf("Please enter keyword: ");
                gets(keyword);

                cleanString(keyword);
                makeLower(keyword);

                makeAlphabetTable(keyword, keyTable);

                playfairDecrypt(text, keyTable);

                printf("Decrypted text: %s\n", text);
            }
            else if(choice == 0)
            {
                break;
            }
            else
            {
                printf("\nInvalid choice");
                choice = -1;
            }
        }
        else
        {
            while ((getchar()) != '\n');
            printf("\nInvalid choice");
            choice = -1;
        }

        
    }

    return 0;
}