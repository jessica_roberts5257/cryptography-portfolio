## AUTHOR: 
Jessica Roberts

## ABOUT:
The purpose of this program is to encrypt and decrypt data using the RSA cryptosystem.

## REQUIREMENTS:
Python 3.8+
Cryptomath Library

## HOW TO USE:
To run this program using a terminal, use the command "python rsa_algorithm.py" for Windows and "python3 rsa_algorithm.py" for Linux. Upon start, the user will be prompted to enter integer data to be encrypted. This integer data can consist of any length of integer, but cannot contain any text values. Upon entering, the program will encrypt the text according to the rsa algorithm and then output the generated public key, the generated private key, and the resulting ciphertext. It will also then decrypt the ciphertext that was just created and output the resulting plaintext.