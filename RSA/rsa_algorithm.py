from cryptomath import functions
import random

'''
Author: Jessica Roberts
Class: Cryptography
Known Bugs: None
Description: This program encrypts and decrypts data using the RSA encryption
 algorithm.
'''

'''
Input: None
Output: a dictionary
Description: This function generates two large, random primes from which it
 calculates important values used in the encryption function for RSA.
'''
def generate_key():
    primes = list()

    while (len(primes) != 2):
        temp = functions.random_prime(513)
        
        if functions.is_prime(temp, 0.001):
            primes.append(temp)

    p, q = primes[0], primes[1]
    n = p * q
    phi = (p - 1) * (q - 1)

    d = -1
    while (d == -1):
        e = random.randint(phi // 2, phi)
        d = functions.findModInverse(e, phi)

    return {'p': p, 'q': q, 'e': e, 'd': d, 'n': n}

'''
Input: info, an integer; keys, a dictionary
Output: an integer
Description: This function uses the public key values to encrypt data
 using the RSA algorithm.
'''
def rsa_encrypt(info, keys):
    e, n = keys['e'], keys['n']

    if info < n:
        temp = functions.modPow(info, e, n)
        return temp
    else:
        return -1

'''
Input: info, an integer; keys, a dictionary
Output: an integer
Description: This function uses the private key values to decrypt data
 using the RSA algorithm.
'''
def rsa_decrypt(info, keys):
    d, n = keys['d'], keys['n']

    temp = functions.modPow(info, d, n)
    return temp

def main():
    
    userdata = "a"
    while(isinstance(userdata, int) == False):
        try:
            userdata = int(input("Please enter integer data to encrypt: "))
        except ValueError:
            print("Please enter integer data only.")

    info = generate_key()
    e, d, n = info['e'], info['d'], info['n']

    print("\nYour public key is:")
    print("\ne:")
    print(e)
    print("\nn:")
    print(n)

    print("\nYour private key is:")
    print("\nd:")
    print(d)
    print("\nn:")
    print(n)

    ciphertext = rsa_encrypt(userdata, info)
    plaintext = rsa_decrypt(ciphertext, info)
    print("\nData:")
    print(userdata)
    print("\nEncrypted data:")
    print(ciphertext)
    print("\nDecrypted data:")
    print(plaintext)

main()