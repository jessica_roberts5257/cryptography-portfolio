## AUTHOR: 
Jessica Roberts

## ABOUT:
The purpose of this program is to encrypt and decrypt data using the simplified DES algorithm.

## REQUIREMENTS:
Latest version of Java

## HOW TO USE:
To run this program using a terminal, compile the program using the command "javac simpleDES.java" and then run the program using "java simpleDES." Upon start, the user will be prompted to enter 8-bit binary data to be encrypted. Binary data consists of onlys 1's and 0's, and 8-bit means there are 8 total characters. An example of the desired input format looks like this: "1 0 0 1 0 1 1 0." Any other input will cause an error. Next, the user will be prompted to enter a 10-bit binary key. An example of the desired input format looks like this: "1 1 0 0 0 0 1 1 0 1." Upon entering, the program will encrypt the text, and then output the resulting ciphertext. It will also then decrypt the ciphertext that was just created and output the resulting plaintext.