import java.io.*;
import java.util.Scanner;
 
/******************************************************************************
 * Author: Jessica Roberts
 * Class: Cryptography
 * Known Bugs: None
 * Description: This program encrypts and decrypts data using the simplified
 * DES algorithm. 
 *****************************************************************************/

public class simpleDES {

    static int[] p10 = {3, 5, 2, 7, 4, 10, 1, 9, 8, 6};
    static int[] p8 = {6, 3, 7, 4, 8, 5, 10, 9};
    static int[] p4 = {2, 4, 3, 1};
    static int[] initPer = {2, 6, 3, 1, 4, 8, 5, 7};
    static int[] extPer = {4, 1, 2, 3, 2, 3, 4, 1};
    static int[] invInitPer = {4, 1, 3, 5, 7, 2, 8, 6};

    static int[][] sBox1 = {{ 1, 0, 3, 2 }, { 3, 2, 1, 0 }, { 0, 2, 1, 3 }, { 3, 1, 3, 2 }};
    static int[][] sBox2 = {{ 0, 1, 2, 3 }, { 2, 0, 1, 3 }, { 3, 0, 1, 0 }, { 2, 1, 0, 3 }};

    static int[] k1 = new int[8];
    static int[] k2 = new int[8];

/******************************************************************************
 * Input: arr, an integer array; n, an integer
 * Output: an integer array
 * Description: This function shift an array of integers left by a specified
 * amount. 
 *****************************************************************************/
    public static int[] shiftLeftN(int[] arr, int n) {

        while (n > 0) {
            int temp = arr[0];

            for(int i = 0; i < (arr.length - 1); i++) {
                arr[i] = arr[i + 1];
            }
            arr[arr.length - 1] = temp;
            n--;
        }

        return arr;
    }

/******************************************************************************
 * Input: x, an integer
 * Output: a string
 * Description: This function converts a decimal integer from 0-3 to its binary
 * value. 
 *****************************************************************************/
    public static String decToBin(int x) {
        String result = "";

        if (x == 0) 
            result = "00";
        else if (x == 1)
            result = "01";
        else if (x == 2)
            result = "10";
        else 
            result = "11";

        return result;
    }

/******************************************************************************
 * Input: arr, an integer array; n, an integer
 * Output: an integer array
 * Description: This function swaps both halves of an 8-bit integer. 
 *****************************************************************************/
    public static int[] nibbleSwap(int[] arr, int n) {

        int[] result = new int[2 * n];
        int[] left = new int[n];
        int[] right = new int[n];

        for(int i = 0; i < n; i++) {
            left[i] = arr[i];
            right[i] = arr[i + n];
        }

        for(int i = 0; i< n; i++) {
            result[i] = right[i];
            result[i + n] = left[i];
        }

        return result;
    }

/******************************************************************************
 * Input: key, an integer array
 * Output: None
 * Description: This function creates a left and right key from a 10-bit
 * specified key. 
 *****************************************************************************/
    public static void createKeys(int[] key) {

        int[] temp = new int[10];

        for(int i = 0; i < 10; i++) {
            temp[i] = key[p10[i] - 1];
        }

        int leftShift[] = new int[5];
        int rightShift[] = new int[5];

        for(int i = 0; i < 5; i++) {
            leftShift[i] = temp[i];
            rightShift[i] = temp[i + 5];
        }

        int[] leftShift2 = shiftLeftN(leftShift, 1);
        int[] rightShift2 = shiftLeftN(rightShift, 1);

        for(int i = 0; i < 5; i++) {
            temp[i] = leftShift2[i];
            temp[i + 5] = rightShift2[i];
        }

        for(int i = 0; i < 8; i++) {
            k1[i] = temp[p8[i] - 1];
        }

        int[] leftShift3 = shiftLeftN(leftShift2, 2);
        int[] rightShift3 = shiftLeftN(rightShift2, 2);

        for(int i = 0; i < 5; i++) {
            temp[i] = leftShift3[i];
            temp[i + 5] = rightShift3[i];
        }

        for(int i = 0; i < 8; i++) {
            k2[i] = temp[p8[i] - 1];
        }
    }

/******************************************************************************
 * Input: arr, an integer array; key, an integer array
 * Output: an integer array
 * Description: This function performs the expansion permutation, s box
 * substitution, and P4 permutation that take place in the f function of the
 * DES algorithm. 
 *****************************************************************************/
    public static int[] desAlgorithm(int[] arr, int[] key) {
        int row = 0;
        int col = 0;
        int loc = 0;

        int[] result = new int[8];

        int[] left = new int[4];
        int[] right = new int[4];

        for(int i = 0; i < 4; i++) {
            left[i] = arr[i];
            right[i] = arr[i + 4];
        }

        int[] temp = new int[8];

        for(int i = 0; i < 8; i++) {
            temp[i] = right[extPer[i] - 1];
        }

        for(int i = 0; i < 8; i++) {
            arr[i] = key[i] ^ temp[i];
        }

        int[] right2 = new int[4];
        int[] left2 = new int[4];

        for(int i = 0; i < 4; i++) {
            left2[i] = arr[i];
            right2[i] = arr[i + 4];
        }

        row = Integer.parseInt("" + left2[0] + left2[3], 2);
        col = Integer.parseInt("" + left2[1] + left2[2], 2);

        loc = sBox1[row][col];

        String leftBin = decToBin(loc);
 
        row = Integer.parseInt("" + right2[0] + right2[3], 2);
        col = Integer.parseInt("" + right2[1] + right2[2], 2);

        loc = sBox2[row][col];

        String rightBin = decToBin(loc);
 
        int[] right3 = new int[4];
        for (int i = 0; i < 2; i++) {
            char tempChar = leftBin.charAt(i);
            char tempChar2 = rightBin.charAt(i);
            right3[i] = Character.getNumericValue(tempChar);
            right3[i + 2] = Character.getNumericValue(tempChar2);
        }

        int[] temp2 = new int[4];
        for (int i = 0; i < 4; i++) {
            temp2[i] = right3[p4[i] - 1];
        }
 
        for (int i = 0; i < 4; i++) {
            left[i] = left[i] ^ temp2[i];
        }
 
        for (int i = 0; i < 4; i++) {
            result[i] = left[i];
            result[i + 4] = right[i];
        }

        return result;
    }

/******************************************************************************
 * Input: plain, an integer array
 * Output: an integer array
 * Description: This function uses helper functions to encrypt text according
 * to the DES algorithm. 
 *****************************************************************************/
    public static int[] desEncrypt(int[] plain) {
        int[] result = new int[8];
        int[] temp = new int[8];

        for(int i = 0; i < 8; i++) {
            temp[i] = plain[initPer[i] - 1];
        }

        int[] temp2 = desAlgorithm(temp, k1);
        int n = temp2.length / 2;
        int[] temp2Swap = nibbleSwap(temp2, n);
        int[] temp3 = desAlgorithm(temp2Swap, k2);

        for(int i = 0; i < 8; i++) {
            result[i] = temp3[invInitPer[i] - 1];
        }

        return result;
    }

/******************************************************************************
 * Input: cipher, an integer array
 * Output: an integer array
 * Description: This function reverses the encryption process according to the
 * DES algorithm.
 *****************************************************************************/
    public static int[] desDecrypt(int[] cipher) {
        int[] result = new int[8];

        int[] temp = new int[8];

        for(int i = 0; i < 8; i++) {
            temp[i] = cipher[initPer[i] - 1];
        }

        int[] temp2 = desAlgorithm(temp, k2);
        int n = temp2.length / 2;
        int[] temp2Swap = nibbleSwap(temp2, n);
        int[] temp3 = desAlgorithm(temp2Swap, k1);

        for(int i = 0; i < 8; i++) {
            result[i] = temp3[invInitPer[i] - 1];
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter 8-bit binary plaintext (Separate each bit with a space): ");
        String input = scan.nextLine();
        String[] clean = input.split(" ");

        int size = clean.length;
        int[] plaintext = new int[size];

        for(int i = 0; i < size; i++) {
            plaintext[i] = Integer.parseInt(clean[i]);
        } 

        System.out.println("Enter 10-bit binary key (Separate each bit with a space): ");
        String keyInput = scan.nextLine();
        String[] cleanKey = keyInput.split(" ");

        int keySize = cleanKey.length;
        int[] key = new int[keySize];

        for(int i = 0; i < keySize; i++) {
            key[i] = Integer.parseInt(cleanKey[i]);
        }
        
        //Create keys

        createKeys(key);

        int[] ciphertext = desEncrypt(plaintext);

        System.out.println("\nEncrypted text: ");

        for(int i = 0; i < 8; i++) {
            System.out.print(ciphertext[i] + " ");
        }

        int[] decryptedText = desDecrypt(ciphertext);

        System.out.println("\nDecrypted text: ");

        for(int i = 0; i < 8; i++) {
            System.out.print(decryptedText[i] + " ");
        }

        scan.close();
    }
}