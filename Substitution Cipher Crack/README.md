## AUTHOR: 
Jessica Roberts

## ABOUT:
The purpose of this program is to launch an attack on a multi-alphabet substitution cipher, such as a vigenere cipher, by using letter frequencies. The program will decrypt a ciphertext by using the letter frequencies of the cipher to determine the most-likely key length, and then using that key length to determine the key used to encrypt the text. After this, it can decrypt the ciphertext completely.

## REQUIREMENTS:
C++ IDE or G++ Compiler

## HOW TO USE:
**NOTE**:
The ciphertext must be at least 500 characters to be decrypted in this way. Otherwise, the program will not have enough letters to determine proper frequencies. Also, it is best to use text that was encrypted with a keyword of eight or fewer characters.


This program uses command line arguments to read large amounts of text from files. To begin using this program, place a text file containing the encrypted text (no punctuation, spaces, and all uppercase) into the same folder as main.cpp.

Using an IDE, go to the debugging menu and put the name of your file into the command line arguments tab.

To compile using a terminal, navigate to location of main.cpp, and enter "g++ -o <executable_name> main.cpp". Then, enter "./<executable_name> filename.txt".
