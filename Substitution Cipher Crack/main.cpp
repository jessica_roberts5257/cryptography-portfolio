#include "iostream"
#include "fstream"
#include "string"
#include "algorithm"
#include "cmath"
#include "vector"

/******************************************************************************
* Author: Jessica Roberts
* Class: Cryptography
* Known Bugs: None
* Description: The purpose of this program is to launch an attack on ciphers
* encrypted using a multi-alphabet substitution cipher, e.g. a vigenere cipher,
* by finding the most-likely key length, then the key, and then decrypting the
* text.
******************************************************************************/

using namespace std;

const char ALPHABET[26] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

void frequency_Analysis(string input, double arr[]);
int next_Max_Index(double arr[], int size, int curMax, int maxCol);
double find_AVG_IC(string input, char** table, int row, int col);
void find_OPT_IC(string input);
char** create_Table(string input, int row, int col);
void left_By_One(double arr[], int size);
void left_Rotate(double arr[], int size, int n);
string find_Key(string input, char** charTable, int row, int col);
char find_Key_Letter(string col);
string create_Key(string input, string key);
string decrypt_With_Key(string cipherText, string key);

int main(int argc, char* argv[])
{
    ifstream fin;
    string input;

    if (argc != 2)
    {
        cout << "Usage: Substitution Cipher Crack.exe inputfile" << endl;
        return -1;
    }

    fin.open(argv[1]);

    if (!fin.is_open())
    {
        cout << "File " << argv[1] << " failed to open." << endl;
        fin.close();
        return -1;
    }

    fin >> input;

    transform(input.begin(), input.end(), input.begin(), ::tolower);

    find_OPT_IC(input);

    return 0;
}

/******************************************************************************
* Input: input, a string; arr, an array of doubles
* Output: None
* Description: This functions calculates the frequency of letters in a string
* and puts the frequencies into an array of doubles in alphbabetical order.
******************************************************************************/
void frequency_Analysis(string input, double arr[])
{

    for (int i = 0; i < input.length(); i++)
    {
        int charValue = (int)input[i] - 97;
        if (charValue >= 0 && charValue <= 25)
            arr[charValue]++;
    }

}

/******************************************************************************
* Input: arr, an array of doubles; size, an integer; curMax, an integer;
* maxCol, an integer
* Output: an integer
* Description: This functions finds the next highest value in an array in
* relation to the current high value.
******************************************************************************/
int next_Max_Index(double arr[], int size, int curMax, int maxCol)
{
    int index = 0;

    for (int i = 0; i < size; i++)
    {
        if ((arr[i] == curMax) && i > maxCol)
        {
            index = i;
            break;
        }
        else if ((arr[i] > arr[index]) && (arr[i] < curMax))
            index = i;
    }

    return index;
}

/******************************************************************************
* Input: arr, an array of doubles; size, an integer
* Output: None
* Description: This functions shifts an array left by one.
******************************************************************************/
void left_By_One(double arr[], int size)
{
    int temp = arr[0];

    for (int i = 0; i < size; i++)
    {
        arr[i] = arr[i + 1];
    }

    arr[size - 1] = temp;
}

/******************************************************************************
* Input: arr, an array of doubles; size, an integer; n, an integer
* Output: None
* Description: This functions shifts an array left by a specified amount.
******************************************************************************/
void left_Rotate(double arr[], int size, int n)
{
    for (int i = 0; i < n; i++)
        left_By_One(arr, size);
}

//Turns the ciphertext into a table of keylength columns
/******************************************************************************
* Input: input, a string; row, an integer; col, an integer
* Output: 2D character array
* Description: This functions creates a 2D character array of a specified
* size and fills it with the input text.
******************************************************************************/
char** create_Table(string input, int row, int col)
{
    int size = input.size();
    long int loc;

    char** table = new char* [col];

    for (int i = 0; i < col; i++)
        table[i] = new char[row];


    for (int j = 0; j < col; j++)
    {
        for (int k = 0; k < row; k++)
        {
            loc = k * col + j;
            if (loc < input.size())
            {
                table[j][k] = input[loc]; //k * width + j
            }
            else
            {
                //Filler values to make table square
                table[j][k] = '0';
            }
        }
    }

    return table;
}

/******************************************************************************
* Input: input, a string; table, a 2D character array; row, an integer;
* col, an integer
* Output: An integer
* Description: This functions calculates the average index of coincidence
* of letter frequencies in a column of text.
******************************************************************************/
double find_AVG_IC(string input, char** table, int row, int col)
{
    double sum = 0;
    double c = 26.0;
    double ic = 0;
    double arr[26];

    string temp;

    for (int i = 0; i < col; i++)
    {
        double x = 0.0;
        long int y = 0;
        double z = 0.0;
        for (int i = 0; i < 26; i++)
            arr[i] = 0;

        //Separate columns into an array
        for (int j = 0; j < row; j++)
        {
            long int loc = j * col + i;
            if (loc < input.size())
            {
                temp.push_back(table[i][j]);
            }
        }

        int size = temp.size();

        frequency_Analysis(temp, arr);

        for (int i = 0; i < 26; i++)
        {
            x += arr[i] * (arr[i] - 1);
        }
        //Calculate IC
        y = size * (size - 1);
        z = x / (y / c);
        sum += z;
        temp.clear();
    }

    sum = sum / col;

    return sum;
}

/******************************************************************************
* Input: input, a string
* col, an integer
* Output: None
* Description: This functions uses helper functions to determine which key
* length yields the optimum index of coinsidence. From that key length, it
* determines which letter derived each column of text to find the key, and then
* uses that key to decrypt the ciphertext.
******************************************************************************/
void find_OPT_IC(string input)
{
    double engIC = 1.73;
    int size = input.size();
    int col = 0;
    int row = 0;
    char** charTable;
    double icArr[8];
    double minDiff = 99999.0;
    int optKeyNum = 0;

    for (int i = 0; i < 8; i++)
        icArr[i] = 0.0;

    for (int i = 0; i < 8; i++)
    {
        col = i + 1;
        row = size / col + (size % col != 0);

        charTable = create_Table(input, row, col);
        icArr[i] = find_AVG_IC(input, charTable, row, col);

        if (abs(icArr[i] - engIC) < minDiff)
            minDiff = abs(icArr[i] - engIC);
    }

    //Determines which key length yielded the best IC
    for (int i = 0; i < 8; i++)
    {
        if (abs(icArr[i] - engIC) == minDiff)
        {
            optKeyNum = i + 1;
        }
    }

    if (optKeyNum == 8)
    {
        if ((abs(icArr[3] - icArr[7])) <= 0.01)
            optKeyNum = 4;
    }

    //Separates ciphertext into keylength number of columns
    col = optKeyNum;
    row = size / col + (size % col != 0);
    charTable = create_Table(input, row, col);

    string cipherText = input;

    for (int i = 0; i < input.size(); i++)
        cipherText[i] = toupper(input[i]);

    cout << "Input: " << cipherText << endl;

    cout << "\n";

    cout << "Key Length: " << optKeyNum << endl;

    cout << "\n";

    string keyword = find_Key(input, charTable, row, col);

    cout << "Keyword: " << keyword << endl;

    cout << "\n";

    string key = create_Key(cipherText, keyword);

    string plainText = decrypt_With_Key(cipherText, key);

    cout << "Decrypted Text: " << plainText << endl;
}

/******************************************************************************
* Input: input, a string; charTable, a 2D character array; row, an integer;
* col, an integer
* Output: a string
* Description: This functions finds which letter was likely used to shift
* each column of ciphertext to compile the encryption key.
******************************************************************************/
string find_Key(string input, char** charTable, int row, int col)
{
    string key = "";
    string temp;
    double sum;
    char a;

    for (int i = 0; i < col; i++)
    {
        sum = 0.0;
        a = ' ';

        for (int j = 0; j < row; j++)
        {
            int loc = j * col + i;
            if (loc < input.size())
            {
                temp.push_back(charTable[i][j]);
            }

        }

        a = find_Key_Letter(temp);
        key.push_back(a);

        temp.clear();
    }

    return key;
}

/******************************************************************************
* Input: col, a string
* Output: a character
* Description: This functions uses a frequency analysis attack to crack the
* caesar cipher used to encrypt a column of ciphertext and determines which
* letter of the alphabet was used to shift it.
******************************************************************************/
char find_Key_Letter(string col)
{
    char keyLetter = ' ';
    double temp[26]; //Array to hold shifted colArr
    double colArr[26]; //Array of column frequencies
    int maxCol = 0; //Index of highest letter frequency in array
    int shiftValue = 0; //Most likely key used to shift this column
    int currentMax = 99999; //Current highest frequency in a column
    int rotateVal = 0;
    int x = 0; //index of x frequency
    int y = 0; //index of y frequency

    //Initialize array for column frequencies
    for (int i = 0; i < 26; i++)
    {
        colArr[i] = 0.0;
    }

    frequency_Analysis(col, colArr);

    int size = sizeof(colArr) / sizeof(colArr[0]);

    maxCol = next_Max_Index(colArr, size, currentMax, maxCol);

    x = size - 3;
    y = size - 1;

    //Shift cipher column 0-26 times to find most likely shift value
    for (int i = 0; i < 26; i++)
    {
        //Copy of colArr for shifting
        for (int i = 0; i < 26; i++)
        {
            temp[i] = colArr[i];
        }

        currentMax = colArr[maxCol];

        rotateVal = (maxCol - 4) % 26;

        left_Rotate(temp, size, rotateVal);

        if (temp[x] == 0 && temp[y] == 0)
        {
            shiftValue = rotateVal;
            break;
        }

        maxCol = next_Max_Index(colArr, size, currentMax, maxCol);
    }

    keyLetter = ALPHABET[shiftValue];

    return keyLetter;
}

/******************************************************************************
* Input: input, a string; key, a string
* Output: a string
* Description: This function repeats the keyword until it's the length of the
* ciphertext.
******************************************************************************/
string create_Key(string input, string key)
{
    for (int i = 0; i < key.size(); i++)
    {
        key[i] = toupper(key[i]);
    }

    int size = input.size();

    for (int i = 0; i < size; i++)
    {
        if (size == i)
            i = 0;
        if (key.size() == input.size())
            break;
        key.push_back(key[i]);
    }

    return key;
}

/******************************************************************************
* Input: ciphertext, a string; key, a string
* Output: a string
* Description: This functions uses the discovered key to decrypt the ciphertext
* according to the vigenere algorithm.
******************************************************************************/
string decrypt_With_Key(string cipherText, string key)
{
    string result;

    for (int i = 0; i < cipherText.size(); i++)
    {
        char x = (cipherText[i] - key[i] + 26) % 26;

        x += 'A';
        result.push_back(x);
    }

    return result;
}