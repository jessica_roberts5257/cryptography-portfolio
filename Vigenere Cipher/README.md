## AUTHOR: 
Jessica Roberts

## ABOUT:
The purpose of this program is to encrypt or decrypt a string of text using the vigenere cipher. To decrypt, you must know the key used to encrypt the ciphertext.

## REQUIREMENTS:
G++ Compiler or C++ IDE

## HOW TO USE:
If using G++, open a terminal, navigate to the location of main.cpp, and enter "g++ -o <executable_name> main.cpp". Then, enter "./<executable_name>". If using a C++ IDE, choose to run without debugging and enter the file into the debugging properties. Upon start, the user will be prompted with three choices: encrypt text, decrypt text, and quit.

Choosing to encrypt will prompt the user for a sentence or word to encrypt. This sentence can have any amount of spacing, punctuation, or capitalization. Once entered, the user will be prompted for a keyword. Single, short words work best as keywords. The keyword can have any amount of spacing, punctuation, or capitalization. After entering, the program will encrypt the entered text using the given keyword and output the resulting encrypted text.

Choosing to decrypt will prompt the user to enter ciphertext that was encrypted using the vigenere cipher. This is only a decryption and not an attack, so the user must know the keyword used to encrypt the ciphertext. The program also assumes that the ciphertext has had all spaces/punctuation removed and is in all uppercase. After entering the ciphertext, the user will be prompted to enter the keyword used for encryption. Once entered, the program will decrypt the text and output the resulting plaintext.

Choosing to quit will end the program.