#include "string"
#include "iostream"
#include "fstream"
#include "algorithm"

/******************************************************************************
* Author: Jessica Roberts
* Class: Cryptography
* Known Bugs: None
* Description: The purpose of this program is to encrypt or decrypt strings of
* text with the vigenere cipher. To decrypt text, the key used to encrypt it
* must be known.
* ****************************************************************************/

using namespace std;

string clean_Input(string input);
string create_Key(string input, string key);
string encrypt_Text(string input, string key);
string decrypt_Text(string input, string key);

int main(int argc, char* argv[])
{
    string quitChar = "T";

    //Program Menu
    while (!((quitChar == "C") || (quitChar == "c")))
    {
        cout << "A) Encrypt Plaintext\nB) Decrypt Ciphertext\nC) Quit\n" << endl;
        cout << "Please enter your choice: ";
        getline(cin, quitChar);

        //Encrypt Text
        if (quitChar == "A" || quitChar == "a")
        {
            string plain, keyword, key, cipher;

            cout << "\nPlease enter plaintext: ";
            getline(cin, plain);

            cout << "\nPlease enter one-word encyption key: ";
            getline(cin, keyword);

            plain = clean_Input(plain);

            keyword = clean_Input(keyword);

            key = create_Key(plain, keyword);

            cipher = encrypt_Text(plain, key);

            cout << "\nEncrypted Text: " << cipher << endl;
        }
        //Decrypt Text
        else if (quitChar == "B" || quitChar == "b")
        {
            string cipher, keyword, plain, key;

            cout << "\nPlease enter ciphertext: ";
            getline(cin, cipher);

            cout << "\nPlease enter keyword used to encrypt text: ";
            getline(cin, keyword);

            keyword = clean_Input(keyword);

            key = create_Key(cipher, keyword);

            plain = decrypt_Text(cipher, key);

            cout << "\nPlain text: " << plain << endl;
        }
    }

    return 0;
}

/******************************************************************************
* In: input, a string of text.
* Out: input, the string passed into the function after being cleaned
* Description: This function takes any string of text and removes all spacing
* and punctuation, as well as converting every letter to a capital.
* ****************************************************************************/
string clean_Input(string input)
{
    int length = input.size();

    for (int i = 0; i < length; i++)
    {
        if (ispunct(input[i]))
        {
            input.erase(i--, 1);
            length = input.size();
        }
    }

    input.erase(remove(input.begin(), input.end(), ' '), input.end());

    for (int i = 0; i < input.size(); i++)
    {
        input[i] = toupper(input[i]);
    }

    return input;
}

/******************************************************************************
* In: input, a string of text; key, a one word key
* Out: key, the one word key now the length of input string
* Description: This function takes in a string of plaintext and the desired
* key and makes the key repeat itself until it is the length of the input text
* and returns the new key.
* ****************************************************************************/
string create_Key(string input, string key)
{
    int size = input.size();

    for (int i = 0; i < size; i++)
    {
        if (size == i)
            i = 0;
        if (key.size() == input.size())
            break;
        key.push_back(key[i]);
    }

    return key;
}

/******************************************************************************
* In: input, a string of text; key, repeated keyword that is length of input
* Out: encrypted_text, the encrypted ciphertext
* Description: This function takes the cleaned plaintext and the generated key
* and encrypts it using the vigenere cipher.
* ****************************************************************************/
string encrypt_Text(string input, string key)
{
    string encrypted_Text;

    for (int i = 0; i < input.size(); i++)
    {
        char x = (input[i] + key[i]) % 26;

        x += 'A';

        encrypted_Text.push_back(x);
    }

    return encrypted_Text;
}

/******************************************************************************
* In: input, a string of encrypted text; key, repeated keyword that is length
* of input
* Out: plain_text, the decrypted text
* Description: This function takes a string of encrypted text and the generated
* key based on the keyword and decrypts it to plaintext.
* ****************************************************************************/
string decrypt_Text(string input, string key)
{
    string plain_text;

    for (int i = 0; i < input.size(); i++)
    {
        char x = (input[i] - key[i] + 26) % 26;

        x += 'A';
        plain_text.push_back(x);
    }

    return plain_text;
}